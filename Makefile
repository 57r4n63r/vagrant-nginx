#!/bin/bash

start: start_vagrant start_nginx

restart: stop_vagrant start

stop: stop_vagrant

destroy: 
    vagrant destroy

stop_vagrant:
	vagrant halt

start_vagrant:
	vagrant up

start_nginx:
	vagrant ssh -c "sudo service nginx restart"

.PHONY: start_nginx start_vagrant start destroy
