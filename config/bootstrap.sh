#!/usr/bin/env bash
if [ -f "/vagrant/config.sh" ]; then
    . /vagrant/config.sh
else
   echo "Config file missing."
fi


sudo add-apt-repository ppa:ondrej/php -y > /dev/null
sudo apt-get update > /dev/null
sudo apt-get -y install git > /dev/null

# nginx
echo "Installing Nginx"
    sudo apt-get -y install nginx > /dev/null
    sudo service nginx start > /dev/null

echo "Installing PHP"
    sudo apt-get install php7.0 php7.0-cli php7.0-fpm -y > /dev/null
    sudo apt-get install curl php7.0-curl php7.0-zip php7.0-gd php7.0-intl php7.0-mbstring php7.0-mcrypt php7.0-mysql php7.0-xml php7.0-soap -y > /dev/null
    sudo apt-get install openssl -y > /dev/null
    sudo apt-get install composer -y

echo "Installing Mysql"
    #mysql configurations
    sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password password ${MYSQL_PASSWORD}'
    sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password ${MYSQL_PASSWORD}'
    sudo apt-get install mariadb-server -y > /dev/null
    sudo sed -i.bak 's/^bind-address/#bind-address/g' /etc/mysql/my.cnf
    sudo service mysql start
    mysql --user=root --password=${MYSQL_PASSWORD} -e "GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' IDENTIFIED BY '${MYSQL_PASSWORD}' WITH GRANT OPTION; FLUSH PRIVILEGES;" 

if [ "$IMPORT_DATABASE" = true ] ; then
    echo "Importing databse ${DATABASE_NAME}"
        mysql --user=root --password=${MYSQL_PASSWORD} -e "create database ${DATABASE_NAME};"
        mysql --user=root --password=${MYSQL_PASSWORD} ${DATABASE_NAME} < /vagrant/${DATABASE_FILE}
fi

echo "set up nginx server & php configurations"
    sudo rm /etc/nginx/sites-available/default
    sudo cp /vagrant/config/nginx/site.conf /etc/nginx/sites-available/site.conf
    sudo sed -i "s/user www-data/user vagrant/" /etc/nginx/nginx.conf
    sudo chmod 644 /etc/nginx/sites-available/site.conf
    sudo ln -s /etc/nginx/sites-available/site.conf /etc/nginx/sites-enabled/site.conf
    sudo rm /etc/php/7.0/fpm/php.ini
    sudo cp /vagrant/config/php/fpm/php.ini /etc/php/7.0/fpm/php.ini
    sudo chmod 644 /etc/php/7.0/fpm/php.ini
    sudo sed -i "s/user = www-data/user = vagrant/" /etc/php/7.0/fpm/pool.d/www.conf
    sudo sed -i "s/group = www-data/group = vagrant/" /etc/php/7.0/fpm/pool.d/www.conf
    sudo sed -i "s/listen.owner = www-data/listen.owner = vagrant/" /etc/php/7.0/fpm/pool.d/www.conf
    sudo sed -i "s/listen.group = www-data/listen.group = vagrant/" /etc/php/7.0/fpm/pool.d/www.conf
    sudo chown -R vagrant:vagrant /run/php

echo "Restarting services..."
    sudo service nginx restart
    sudo systemctl start php7.0-fpm

echo "symlink /var/www/html => /vagrant/www"
    sudo rm -rf /var/www/html
    sudo ln -s /vagrant/www /var/www/html
    sudo chown vagrant:vagrant /var/www/
    sudo chmod 644 /var/www/html
